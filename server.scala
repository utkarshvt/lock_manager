package lockserver;

import lockserver.LockService._
import java.io._
import java.net.{InetAddress,ServerSocket,Socket,SocketException}
import java.util.Random

/* Lock request format */
class Request (var op : Int, var Id : Int) extends Serializable

/* Server reply format */
class Reply (var op : Int, var rep : Int) extends Serializable

object randomserver {

  def main(args: Array[String]): Unit = {
    try {
      val listener = new ServerSocket(9999);
      
     /* Iniialize the lock state */

      val wrap = new Wrapper
	 
     while (true)
        new ServerThread(listener.accept(),wrap).start();
      listener.close()
    }
    catch {
      case e: IOException =>
        System.err.println("Could not listen on port: 9999.");
        System.exit(-1)
    }
  }

}

class ServerThread(socket: Socket, wrap : Wrapper) extends Thread("ServerThread") {
	
	 
override def run(): Unit = {
	val rand = new Random(System.currentTimeMillis());
    	try 
	{
      		println("New server thread created")
		val out = new ObjectOutputStream(new DataOutputStream(socket.getOutputStream()));
      		val in = new ObjectInputStream(
        	new DataInputStream(socket.getInputStream()));

      		while (true) 
		{
			val request = in.readObject().asInstanceOf[Request];
	  		var clientReply = new Reply (0,0)
			if(request.op == 0)
			{
				println("Received lock request from Client " + request.Id)	
				val ret = wrap.getLock(request.Id)

				if(ret)
				{
					val reply = new Reply(request.op, 1)
					clientReply = reply
				}
				else
				{
					val reply = new Reply(request.op, 0)
					clientReply = reply
				}				 
			}
			else
			{
				println("Received unlock request from Client " + request.Id )	
				val ret = wrap.putLock(request.Id)
				
				
				if(ret)
				{
					val reply = new Reply(request.op, 1)
					clientReply = reply
				}
				else
				{
					val reply = new Reply(request.op, 0)
					clientReply = reply
				}				 
			}
          	
			
			out.writeObject(clientReply)
        		Thread.sleep(100)
      		}

      	out.close();
      	in.close();
      	socket.close()
    }
    catch 
   {
      case e: SocketException =>
        () // avoid stack trace when stopping a client with Ctrl-C
      case e: IOException =>
        e.printStackTrace();
    }
  }
}

