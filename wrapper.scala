package lockserver;

import lockserver.LockService._

class Wrapper  {
	
	var s: state_ext[Int,Unit]  = state_exta[Int, Unit](None,())			/* State variable associated with lock */
	var lock : AnyRef = new Object()						/* Lock for synchronization */
	
	/* The Lock API */
	def getLock ( id: Int) : Boolean =  lock.synchronized {
		val (s1,x1) =  lockserver.LockService.trans(s,Lock(id))
		s = s1
		val locked = getResponse(x1)
		println(showResponse(x1))
		if(locked)
		{
			println(" Locked by client " + id )
		}
		else
		{
			println(" Lock failed for client " + id)
		}
		
		lock.notifyAll()
		locked
		
	}
	
	/* The unlock API */
	def putLock (id: Int) : Boolean =  lock.synchronized {
		val (s1, x1) = lockserver.LockService.trans(s, Unlock(id))
		s = s1
		val unlocked = getResponse(x1)
		if(unlocked)
		{
			println("UnLocked by client " + id)
		}
		else
		{
			println(" Unlock failed for client " + id)
		}
		
		println(showResponse(x1))
		
		unlocked
	}

	/* Print the response */
	def showResponse [A] (x1 : Option[response]): String = x1 match {
		case Some(s) => s.toString()
		case None => "None"
	}

	/* Translate the response into success or failure */
	def getResponse [A] (x1: Option[response]) : Boolean = x1 match {
		case Some(s) => true
		case None => false
	}

}
/*
object Test {
	def main(args: Array[String]) {
	val s = state_exta[Int, Unit](None,())
	val wrap = new Wrapper
	var (s1,ret) = wrap.wlock(1)
	val(s2,ret1) = wrap.wunlock(1)	
	s1 = s2
	wrap.wunlock(s1,1)	
	
	}
}*/
