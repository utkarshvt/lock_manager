package lockserver;

import java.io._
import java.net.{InetAddress,ServerSocket,Socket,SocketException}
import java.util.Random
import java.util.concurrent.ThreadLocalRandom

/** 
 * Simple client/server application using Java sockets. 
 * 
 * The server simply generates random integer values and 
 * the clients provide a filter function to the server 
 * to get only values they interested in (eg. even or 
 * odd values, and so on). 
 */
object lockclient {
     
  def main(args: Array[String]) {
   
    try {
     for( a <- 0 to 3) 
        new ClientThread().start();
    }
    catch {
      case e: IOException =>
        System.err.println("Could not connect to  port: 9999.");
        System.exit(-1)
    }

  }


class ClientThread extends Thread("ServerThread") {

	override def run(): Unit = {
		
    try {
      
      val longId  = Thread.currentThread().getId()	
      val Id = longId.toInt
      val rand = ThreadLocalRandom.current(); 

      //val request = new Request(0,Id)
      			
      val ia = InetAddress.getByName("localhost")
      val socket = new Socket(ia, 9999)
      val out = new ObjectOutputStream(
        new DataOutputStream(socket.getOutputStream()))
     
     val in = new ObjectInputStream(
                new DataInputStream(socket.getInputStream()));


      //out.writeObject(request)
      //out.flush()

      
      //out.writeObject(request)
      //out.flush()
	

	while (true) 
	{
        	
		
		val op = rand.nextInt(0,100) % 2
		
		val request = new Request(op,Id)
		
		out.writeObject(request)
      		out.flush()
	
		val reply = in.readObject().asInstanceOf[Reply]
        	if(reply.rep == 0)
		{
			if(reply.op == 0)
			{
				println(" Lock operation failed for Client " + Id)
			}
			else
			{
				println(" Unlock operation failed for Client " + Id)
		
			}
		}
		else
		{
			if(reply.op == 0)
			{
				println(" Lock operation suceeded Client " + Id)
			}
			else
			{
				println(" Unlock operation suceeded Client " + Id)
		
			}
		
		}
      
		Thread.sleep(1000)
	}
      	
	out.close()
      	in.close()
      	socket.close()
    	}
    	catch 
	{
      		case e: IOException =>
        	e.printStackTrace()
    	}
	
     }
  }
}
