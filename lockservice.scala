package lockserver;

object LockService {

trait equal[A] {
  val `LockService.equal`: (A, A) => Boolean
}
def equal[A](a: A, b: A)(implicit A: equal[A]): Boolean =
  A.`LockService.equal`(a, b)

implicit def equal_t[T] : LockService.equal[T] = new LockService.equal[T] {
	val `LockService.equal` = (a : T, b: T) => a==b
}


abstract sealed class request[A]
final case class Lock[A](a: A) extends request[A]
final case class Unlock[A](a: A) extends request[A]

abstract sealed class response
final case class Acquired() extends response
final case class Released() extends response

abstract sealed class state_ext[A, B]
final case class state_exta[A, B](a: Option[A], b: B) extends state_ext[A, B]

def eq[A : equal](a: A, b: A): Boolean = equal[A](a, b)

def lockedBy[A, B](x0: state_ext[A, B]): Option[A] = x0 match {
  case state_exta(lockedBy, more) => lockedBy
}

def trans[A : equal](s: state_ext[A, Unit], x1: request[A]):
      (state_ext[A, Unit], Option[response])
  =
  (s, x1) match {
  case (s, Lock(p)) =>
    (lockedBy[A, Unit](s) match {
       case None =>
         (state_exta[A, Unit](Some[A](p), ()), Some[response](Acquired()))
       case Some(_) => (s, None)
     })
  case (s, Unlock(p)) =>
    (lockedBy[A, Unit](s) match {
       case None => (s, None)
       case Some(q) =>
         (if (eq[A](p, q))
           (state_exta[A, Unit](None, ()), Some[response](Released()))
           else (s, None))
     })
}

} /* object LockService */
